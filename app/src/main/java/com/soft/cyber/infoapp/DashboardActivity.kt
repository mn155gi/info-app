package com.soft.cyber.infoapp

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.soft.cyber.infoapp.entities.CreditRepInfo
import com.soft.cyber.infoapp.retrofit.RestAPI
import kotlinx.android.synthetic.main.activity_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardActivity : AppCompatActivity() {

    /**
     * Inits the app
     *      - starts to spin the donut -> displays "Loading data..."
     *      - runs the endpoint call
     *      - sets the onClickListener for the donut
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        showDataInfoToUser(false)
        donutView.spin()
        getReportInfoAndShowResult()
        donutView.setOnClickListener {
            loadDataAgain()
        }
    }

    /**
     * This function calls the endpoint and shows info on GUI
     *      - if the call was successful shows the score and sets the donut progress value
     *      - if not -> shows info for the user that there was a problem
     */
    private fun getReportInfoAndShowResult() {
        donutView.setBarColor(ContextCompat.getColor(applicationContext, R.color.progressStart), ContextCompat.getColor(applicationContext, R.color.progressMidTwo), ContextCompat.getColor(applicationContext, R.color.progressMidOne), ContextCompat.getColor(applicationContext, R.color.progressEnd))
        RestAPI().getReport().enqueue(object : Callback<CreditRepInfo> {
            override fun onResponse(call: Call<CreditRepInfo>?, response: Response<CreditRepInfo>?) {
                if (response != null && response.isSuccessful) {
                    donutView.stopSpinning()
                    showDataInfoToUser(true)
                    val progressPercentage = calculateProgressPercentage(response.body()?.creditReportInfo?.maxScoreValue, response.body()?.creditReportInfo?.score)
                    donutView.setValueAnimated(progressPercentage)
                    // sets the color of the score textView
                    when (progressPercentage) {
                        in 0..25 -> scoreValueTv.setTextColor(ContextCompat.getColor(applicationContext, R.color.progressStart))
                        in 26..50 -> scoreValueTv.setTextColor(ContextCompat.getColor(applicationContext, R.color.progressMidTwo))
                        in 51..75 -> scoreValueTv.setTextColor(ContextCompat.getColor(applicationContext, R.color.progressMidOneDarker))
                        in 76..100 -> scoreValueTv.setTextColor(ContextCompat.getColor(applicationContext, R.color.progressEnd))
                        else -> scoreValueTv.setTextColor(ContextCompat.getColor(applicationContext, R.color.progressStart))
                    }
                    // sets the value of the textViews
                    scoreValueTv.text = response.body()?.creditReportInfo?.score.toString()
                    val footerString = resources.getString(R.string.footer_text) + " " + response.body()?.creditReportInfo?.maxScoreValue.toString()
                    footerTv.text = footerString
                } else {
                    showFailedInfo(false)
                }
            }

            override fun onFailure(call: Call<CreditRepInfo>?, t: Throwable?) {
                showFailedInfo(true)
                t?.printStackTrace()
            }

        })
    }

    /**
     * Called by onClick
     * - loads data again
     */
    private fun loadDataAgain() {
        donutView.spin()
        showDataInfoToUser(false)
        getReportInfoAndShowResult()
    }

    /**
     * Parameter: boolean value
     *      - if true -> shows header, score and footer on GUI and hides loading text view
     *      - if false -> hides header, score and footer and shows loading text view
     * Function for switching between field visibilities on GUI
     */
    private fun showDataInfoToUser(showData: Boolean) {
        if (showData) {
            loadingTv.visibility = View.GONE
            headerTv.visibility = View.VISIBLE
            scoreValueTv.visibility = View.VISIBLE
            footerTv.visibility = View.VISIBLE
        } else {
            loadingTv.visibility = View.VISIBLE
            headerTv.visibility = View.GONE
            scoreValueTv.visibility = View.GONE
            footerTv.visibility = View.GONE
        }
    }

    /**
     * This function is called when connection is refused or failed
     *  - shows error message on GUI
     *  - sets donut progress value to 0f
     */
    private fun showFailedInfo(networkOff: Boolean) {
        if (networkOff) {
            loadingTv.text = resources.getString(R.string.network_off)
        } else {
            loadingTv.text = resources.getString(R.string.failed_to_connect)
        }
        donutView.stopSpinning()
        donutView.setValueAnimated(0f)
        showDataInfoToUser(false)
    }

    /**
     * Parameters: max value and actual value
     * Function returns percentage calculated from max -> (actual / (max / 100))
     * The calculated value is integer but casted to float -> no decimal values will be returned
     */
    private fun calculateProgressPercentage(max: Int?, actual: Int?): Float {
        if (max is Int && actual is Int) {
            return (actual / (max / 100)).toFloat()
        }
        return 0f
    }
}
