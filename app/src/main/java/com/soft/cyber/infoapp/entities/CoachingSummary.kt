package com.soft.cyber.infoapp.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Generated class from JSON to POJO
 *      - converted automatically
 */
class CoachingSummary {
    @SerializedName("activeTodo")
    @Expose
    var activeTodo: Boolean? = null
    @SerializedName("activeChat")
    @Expose
    var activeChat: Boolean? = null
    @SerializedName("numberOfTodoItems")
    @Expose
    var numberOfTodoItems: Int? = null
    @SerializedName("numberOfCompletedTodoItems")
    @Expose
    var numberOfCompletedTodoItems: Int? = null
    @SerializedName("selected")
    @Expose
    var selected: Boolean? = null

}