package com.soft.cyber.infoapp.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Generated class from JSON to POJO
 * - main object returned when called getCreditReportInfo() in ApiService
 * - converted automatically
 */
class CreditRepInfo {

    @SerializedName("accountIDVStatus")
    @Expose
    var accountIDVStatus: String? = null
    @SerializedName("creditReportInfo")
    @Expose
    var creditReportInfo: CreditReportInfo? = null
    @SerializedName("dashboardStatus")
    @Expose
    var dashboardStatus: String? = null
    @SerializedName("personaType")
    @Expose
    var personaType: String? = null
    @SerializedName("coachingSummary")
    @Expose
    var coachingSummary: CoachingSummary? = null
    @SerializedName("augmentedCreditScore")
    @Expose
    var augmentedCreditScore: Any? = null

}