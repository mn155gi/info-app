package com.soft.cyber.infoapp.retrofit

import com.soft.cyber.infoapp.entities.CreditRepInfo
import retrofit2.Call
import retrofit2.http.GET

/**
 * Interface for the endpoints
 */
interface ApiService {

    /**
     * Credit info endpoint
     */
    @GET("cdn.clearscore.com/native/interview_test/creditReportInfo.json")
    fun getCreditReportInfo(): Call<CreditRepInfo>
}
