package com.soft.cyber.infoapp.retrofit

import com.soft.cyber.infoapp.entities.CreditRepInfo
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Class for api communication
 */
class RestAPI {

    private val baseUrl = "https://s3.amazonaws.com/"
    private val apiService: ApiService

    /**
     * Sets up the retrofit client
     *      - added logging for easier debugging
     */
    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    /**
     * Calls the endpoint
     * Returns CreditRepInfo object
     */
    fun getReport(): Call<CreditRepInfo> {
        return apiService.getCreditReportInfo()
    }
}
